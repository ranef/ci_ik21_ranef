<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
<style type="text/css">
body ul li {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	list-style-type: none;
	text-align:center;
	vertical-align:central;
	margin-left:65px;
}
body ul li ul {
	padding: 0px;
	display: none;
	text-align:left;
}
body ul li {
	background-color: #CCC;
	float: left;
	height: 40px;
	width: 150px;
	float:left;
	line-height:40px;
}
body ul li:hover ul {
	display:block;
}
body ul li:hover{
	background-color: #F00;
	top: 20px;
	right: 500px;
	bottom: 50px;
	vertical-align: central;
	text-align: center;
}

body menu {
	font-size: 10px;
}
#menu {
	background-color: #F00;
}
</style>
</head>
<body>
<table width="100%" border="0" align="center">
<tr>
    <td align="center" bgcolor="#006699"><h1>&quot;TOKO PIZZA&quot;</h1></td>
  </tr>
    <td width="100" height="50" align="center" valign="middle" bgcolor="#006699"><ul id="menu" name="menu">
       	<li>Home</li>
            <li>Master
            	<ul>
                	<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
                    <li><a href="<?=base_url();?>menu/listmenu">Data Menu</a></li>

              </ul>
        </li>
            <li>Transaksi
              <ul>
                  <li><a href="<?=base_url();?>pemesanan/listpemesanan">Pembelian</a></li>

              </ul>
            </li>
            <li>Report</li>
            <li>Log Out</li>
    </ul></td>
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10"><h2>&nbsp;</h2></td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Data Menu</h2></td>
      </tr>
      <tr bgcolor="#CCCCCC">
        <td><table width="100%" border="0">
          <tr>
            <td colspan="5" align="left"><a href="<?=base_url();?>menu/inputmenu">Input Data</a></td>
            <td align="right"><input type="text" Name="cari" placeholder="Cari Nama" />
              <input type="submit" name="cari_data" id="cari_data" value="cari data"></td>
            </tr>
          <tr align="center">
            <td bgcolor="#3366FF">No</td>
            <td bgcolor="#3366FF">Kode Menu</td>
            <td bgcolor="#3366FF">Nama Menu</td>
            <td bgcolor="#3366FF">Harga</td>
            <td bgcolor="#3366FF">Keterangan</td>
            <td bgcolor="#3366FF">Aksi</td>
            </tr>
         <?php
		 		$no_urut = 0;
		 		foreach ($data_menu as $data){
				$no_urut++;
		?>
          <tr align="center" bgcolor="#000000">
          <td><font color="#FFFFFF"><?= $no_urut;?></font></td>
            <td><font color="#FFFFFF"><?= $data->kode_menu;?></font></td>
            <td><font color="#FFFFFF"><?= $data->nama_menu;?></font></td>
            <td><font color="#FFFFFF"><?= $data->harga;?></font></td>
            <td><font color="#FFFFFF"><?= $data->keterangan;?></font></td>
            <td><a href="<?=base_url();?>menu/detailMenu/<?=$data->kode_menu;?>"><font color="#FFFFFF" >detail</font></a> &nbsp;&nbsp; <font color="#FFFFFF" >|</font> &nbsp;&nbsp; <a href="<?=base_url();?>menu/editMenu/<?=$data->kode_menu;?>"><font color="#FFFFFF" >edit</font>&nbsp;&nbsp;<font color="#FFFFFF" >|</font> &nbsp;&nbsp; <a href="<?=base_url();?>menu/deleteMenu/<?=$data->kode_menu;?>" onclick="return confirm('Yakin ingin menghapus data?');"><font color="#FFFFFF" >Delete</font> </a></td>
            </tr>
         <?php } ?>
        </table></td>
      </tr>
      <tr>
        <td>
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
  <td>
  
  </td>
  </tr>
  <tr>
    <td>
    	
    </td>
  </tr>
</table>
</body>
</html>