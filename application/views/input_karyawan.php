<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
<style type="text/css">
body ul li {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	list-style-type: none;
	text-align:center;
	vertical-align:central;
	margin-left:65px;
}
body ul li ul {
	padding: 0px;
	display: none;
	text-align:left;
}
body ul li {
	background-color: #CCC;
	float: left;
	height: 40px;
	width: 150px;
	float:left;
	line-height:40px;
}
body ul li:hover ul {
	display:block;
}
body ul li:hover{
	background-color: #F00;
	top: 20px;
	right: 500px;
	bottom: 50px;
	vertical-align: central;
	text-align: center;
}

body menu {
	font-size: 10px;
}
#menu {
	background-color: #F00;
}
</style>
</head>
<body>
<form action="<?=base_url()?>karyawan/inputkaryawan" method="POST">
<table width="100%" border="0" align="center">
<tr>
    <td align="center" bgcolor="#006699"><h1>&quot;TOKO JAYA ABADI&quot;</h1></td>
  </tr>
    <td width="100" height="50" align="center" valign="middle" bgcolor="#006699"><ul id="menu" name="menu">
       	<li>Home</li>
            <li>Master
            	<ul>
                	<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
                    <li><a href="<?=base_url();?>menu/listmenu">Data Menu</a></li>
              </ul>
        </li>
            <li>Transaksi</li>
            <li>Report</li>
            <li>Log Out</li>
    </ul></td>
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10"><h2>&nbsp;</h2></td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Input Karyawan</h2></td>
      </tr>
      <tr>
        <td><table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="">
          <tr>
            <td width="37%">NIK</td>
            <td width="4%"> :</td>
            <td width="59%"><input type="text" name="nik" id="nik" maxlength="50"></td>
          </tr>
          <tr>
            <td>Nama</td>
            <td>:</td>
            <td><input type="text" name="nama" id="nama" maxlength="50"></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><textarea name="alamat" id="alamat" cols="45" rows="5"></textarea></td>
          </tr>
          <tr>
            <td>Telepon</td>
            <td>:</td>
            <td><input type="text" name="telp" id="telp" maxlength="15"></td>
          </tr>
          <tr>
            <td>Tempat Lahir</td>
            <td>:</td>
            <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50"></td>
          </tr>
          <tr>
            <td>Tanggal Lahir</td>
            <td>:</td>
            <td><select name="tgl" id="tgl">
              <?php
      	for($tgl=1;$tgl<=31;$tgl++){
      ?>
              <option value="<?=$tgl;?>">
                <?=$tgl;?>
                </option>
              <?php
       	}
       ?>
              </select>
              <select name="bln" id="bln">
                <?php
      	$bulan_n = array('Januari','Februari','Maret','April',
        				'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
      ?>
                <option value="<?=$bln+1;?>">
                  <?=$bulan_n[$bln];?>
                  </option>
                <?php
        	}
        ?>
                </select>
              <select name="thn" id="thn">
                <?php
      	for($thn = date('Y')-60;$thn<=date('Y')-15;$thn++){
      ?>
                <option value="<?=$thn;?>">
                  <?=$thn;?>
                  </option>
                <?php
      	} 
      ?>
                </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><input type="submit" name="Submit" id="Submit" value="Simpan">
              <input type="reset" name="reset2" id="reset2" value="Reset"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><a href="<?=base_url();?>karyawan"><input type="button" name="Kembali2" id="Kembali2" value="Kembali Ke Menu Sebelumnya"></a></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>