<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Master</title>
<style type="text/css">
body ul li {
	font-family: Arial, Helvetica, sans-serif;
	font-size:16px;
	list-style-type: none;
	text-align:center;
	vertical-align:central;
	margin-left:65px;
}
body ul li ul {
	padding: 0px;
	display: none;
	text-align:left;
}
body ul li {
	background-color: #CCC;
	float: left;
	height: 40px;
	width: 150px;
	float:left;
	line-height:40px;
}
body ul li:hover ul {
	display:block;
}
body ul li:hover{
	background-color: #F00;
	top: 20px;
	right: 500px;
	bottom: 50px;
	vertical-align: central;
	text-align: center;
}

body menu {
	font-size: 10px;
}
#menu {
	background-color: #F00;
}
</style>
</head>
<body>
<form action="<?=base_url()?>pemesanan/listpemesanan/?>" method="POST">
<table width="100%" border="0" align="center">
<tr>
    <td align="center" bgcolor="#006699"><h1>&quot;TOKO PIZZA&quot;</h1></td>
  </tr>
    <td width="100" height="50" align="center" valign="middle" bgcolor="#006699"><ul id="menu" name="menu">
       	<li>Home</li>
            <li>Master
            	<ul>
                	<li><a href="<?=base_url();?>karyawan/listkaryawan">Data Karyawan</a></li>
                    <li><a href="<?=base_url();?>menu/listmenu">Data Barang</a></li>
              </ul>
        </li>
            <li>Transaksi
            	<ul>
                	<li><a href="<?=base_url();?>pemesanan/listpemesanan">Pembelian</a></li>
              </ul>
            </li>
            <li>Report</li>
            <li>Log Out</li>
    </ul></td>
  <tr>
    <td><table width="100%" border="0">
    <tr align="center">
        <td height="10"><h2>&nbsp;</h2></td>
      </tr>
      <tr align="center">
        <td height="80"><h2>Input Pesanan</h2></td>
      </tr>
      <tr>
        <td><table width="1000px" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#999999">
          <tr>
            <td>Nama Karyawan</td>
            <td>:</td>
            <td><select name="nik" id="nik">
              <?php foreach($data_karyawan as $data) {?>
              <option value="<?= $data->nik; ?>">
                <?= $data->nama; ?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Nama Pelanggan</td>
            <td>:</td>
            <td><input type="text" name="nama_pelanggan" id="nama_pelanggan" maxlength="50"></td>
          </tr>
          <tr>
            <td>Nama Menu</td>
            <td>:</td>
            <td><select name="kode_menu" id="kode_menu">
              <?php foreach($data_menu as $data) {?>
              <option value="<?= $data->kode_menu; ?>">
                <?= $data->nama_menu; ?>
                </option>
              <?php } ?>
            </select></td>
          </tr>
          <tr>
            <td>Qty</td>
            <td>:</td>
            <td><input type="text" name="qty" id="qty" /></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td><input type="submit" name="Submit" id="Submit" value="Proses">
              <input type="reset" name="reset" id="reset" value="Reset"></td>
          </tr>
          <tr>
            <td></td>
            <td></td>
            <td><a href="<?=base_url();?>pemesanan/listpemesanan">
              <input type="button" name="Submit" id="Submit" value="Kembali Ke Menu Sebelumnya">
            </a></td>
          </tr>
        </table>
          <table width="100%" border="0">
            <tr align="center">
              <td height="10"><h2>&nbsp;</h2></td>
            </tr>
            <tr bgcolor="#CCCCCC">
              <td><table width="100%" border="0">
                <tr align="center">
                  <td bgcolor="#3366FF">No</td>
                  <td bgcolor="#3366FF">Nik</td>
                  <td bgcolor="#3366FF">Nama Pelanggang</td>
                  <td bgcolor="#3366FF">Qty</td>
                  <td bgcolor="#3366FF">Harga</td>
                  <td bgcolor="#3366FF">Jumlah</td>
                </tr>
                <?php
        $total_harga=0;
		 		$no_urut = 0;
		 		foreach ($data_pesanan as $data)
        {
				$no_urut++;
		?>
                <tr align="center" bgcolor="#000000">
                  <td><font color="#FFFFFF">
                    <?= $no_urut;?>
                    </font></td>
                  <td><font color="#FFFFFF">
                    <?= $data->nik;?>
                    </font></td>
                  <td><font color="#FFFFFF">
                    <?= $data->nama_pelanggan;?>
                    </font></td>
                    <td><font color="#FFFFFF">
                    <?= $data->qty;?>
                    </font></td>
                    <td><font color="#FFFFFF">
                    <?= number_format($data->total_harga);?>
                    </font></td>
                  <td><<font color="#FFFFFF">
                    <?= number_format($data->total_harga);?>
                    </font></td>
                  </tr>
                  <?php
                  // hitung total
                  $total_harga +=$data->total_harga ;
                   } 
                  ?>

                <tr align="center" bgcolor="#000000" >
                  <td colspan="5" align="right" bgcolor="#FFFFFF">TOTAL</td>
                  <td align="right">
                  <font color="#FFFFFF" >
                  Rp. <b><?= number_format($total_harga); ?></b> </font></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
            </tr>
            </form>
          </table>
          <p>&nbsp;</p></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>