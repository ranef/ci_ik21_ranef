<?php defined('BASEPATH') OR exit('No direct script access allowed');


class menu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("menu_model");
		
	}

	public function index()
	{
		$this->listMenu();
	}
	
	public function listMenu()
	{
		$data['data_menu'] =$this->menu_model->tampilDataMenu();
		$this->load->view('list_menu',$data);
	}
	
	public function inputMenu()
	{
		$data['data_menu'] =$this->menu_model->tampilDataMenu();
			if(!empty($_REQUEST)){
				$m_menu=$this->menu_model;
				$m_menu->save();
				redirect("menu/index","refresh");
				}
		$this->load->view('input_menu',$data);	
	}
	
	public function detailMenu($kode_menu)
	{
		$data['detailMenu']=$this->menu_model->detailMenu($kode_menu);
		$this->load->view('detailMenu',$data);
	}

	public function editMenu($kode_menu)
	{
		$data['editMenu']=$this->menu_model->editMenu($kode_menu);
		
		if(!empty($_REQUEST))
		{
			$m_menu=$this->menu_model;
			$m_menu->update($kode_menu);
			redirect("menu/index","refresh");
		}
		$this->load->view('editMenu',$data);
	}

	public function deleteMenu($kode_menu)
	{
		$m_menu=$this->menu_model;
		$m_menu->delete($kode_menu);
		redirect("menu/index","refresh");
	}	
}