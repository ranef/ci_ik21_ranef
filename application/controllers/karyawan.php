nb 7<?php defined('BASEPATH') OR exit('No direct script access allowed');


class karyawan extends CI_Controller
{
	public function __construct()
	{
		//buat panggil scrip pertama kali dijalankan
		parent::__construct();
		//load model terkait
		$this->load->model("karyawan_model");
	}

	public function index()
	{
		$this->listKaryawan();
	}
	
	public function listKaryawan()
	{
		$data['data_karyawan'] =$this->karyawan_model->tampilDataKaryawan();
		$this->load->view('list_karyawan',$data);
	}	
	
	public function inputKaryawan()
	{
		$data['data_karyawan']=$this->karyawan_model->tampilDataKaryawan();
			if(!empty($_REQUEST)){
				$m_karyawan=$this->karyawan_model;
				$m_karyawan->saveDataKaryawan();
				redirect("karyawan/index","refresh");
				}
		
		$this->load->view('input_karyawan',$data);
	}
	
	public function detailKaryawan($nik)
	{
		$data['detailKaryawan']=$this->karyawan_model->detailKaryawan($nik);
		$this->load->view('detailKaryawan',$data);
	}
	
	public function editKaryawan($nik)
	{
		$data['editKaryawan']=$this->karyawan_model->tampilDataKaryawan();

		if(!empty($_REQUEST)){
				$m_karyawan=$this->karyawan_model;
				$m_karyawan->updateKaryawan($nik);
				redirect("karyawan/index","refresh");
				}
		
		$this->load->view('editKaryawan',$data);
	}
}