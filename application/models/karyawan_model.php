<?php defined('BASEPATH') OR exit('No direct script access allowed');


class karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table="master_karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <>nama_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function saveDataKaryawan()
	{
		$tgl=$this->input->post('tgl');
		$bulan=$this->input->post('bln');
		$thn=$this->input->post('thn');
		$tgl_gabung=$thn."-".$bulan."-".$tgl;
		
		$data['nik']	=$this->input->post('nik');
		$data['nama']	=$this->input->post('nama');
		$data['alamat']	=$this->input->post('alamat');
		$data['telp']	=$this->input->post('telp');
		$data['tempat_lahir']	=$this->input->post('tempat_lahir');
		$data['tanggal_lahir']	=$tgl_gabung;
		$data['flag']	=1;
		$this->db->insert($this->_table,$data);
	}
		
	public function detailKaryawan($nik)
	{
		$this->db->select('*');
		$this->db->where('nik',$nik);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	public function editKaryawan($nik)
	{

		$this->db->select('*');
		$this->db->where('nik',$nik);
		$this->db->where('flag',1);
		$result=$this->db->get($this->_table);
		return $result->result();
	}

	/*public function updateKaryawan($nik)
	{
		$tgl=$this->input->post('tgl');
		$bulan=$this->input->post('bln');
		$thn=$this->input->post('thn');
		$tgl_gabung=$thn."-".$bulan."-".$tgl;
		
		
		$data['nik']	=$this->input->post('nik');
		$data['nama']	=$this->input->post('nama');
		$data['alamat']	=$this->input->post('alamat');
		$data['telp']	=$this->input->post('telp');
		$data['tempat_lahir']	=$this->input->post('tempat_lahir');
		$data['tanggal_lahir']	=$tgl_gabung;
		$data['flag']	=1

		$this->db->where('nik',$nik);
		$this->db->update($this->_table,$data);
	}


	public function delete($nik)
	{
		$this->db->where('nik',$nik);
		$this->db->delete($this->_table);

	}
	*/
}
